import glob
import sys
from time import sleep
import serial.tools.list_ports

import serial
from SerialCommunication.ControlUnit import ControlUnit

CONTROL_UNITS = {
    1: {
        'name': 'Control Unit with temperature sensor',
        'update_interval': 60
    },

    2: {
        'name': 'Control Unit with light sensor',
        'update_interval': 60
    }
}


class ControlUnitManager:
    def __init__(self):
        self.control_units = []
        self.tick = 0

        self.on_cu_connect_cb = None  # On new ControlUnit Callback
        self.on_cu_disconnect_cb = None  # On ControlUnit disconnect
        self.on_cu_update_cb = None  # On ControlUnit update

    def update(self):
        self.check_for_disconnected_control_units()  # Check for disconnected CU
        self.update_control_units()  # Update control units

        # Check for new CU
        open_ports = ControlUnitManager.get_open_ports()
        if open_ports:
            self.check_for_new_control_units(open_ports)

    def update_control_units(self):
        self.tick += 1

        if self.on_cu_update_cb:
            for control_unit in self.control_units:
                if self.tick % control_unit.update_interval == 0:
                    self.on_cu_update_cb(control_unit, control_unit.read_sensor())


    def check_for_disconnected_control_units(self):
        for control_unit in self.control_units:
            if not control_unit.is_connected():
                # Remove it
                if self.on_cu_disconnect_cb:
                    self.on_cu_disconnect_cb(control_unit)

                self.control_units.remove(control_unit)

    def check_for_new_control_units(self, ports):
        for port in ports:
            # Check if this port is already a ControlUnit
            if any(control_unit.connection.port == port for control_unit in self.control_units):
                # Already known!
                continue

            print("Detected new device!")

            # Create a serial connection to this port
            try:
                print("Connecting....")
                connection = serial.Serial(port, timeout=1)

                # Create a ControlUnit
                control_unit = ControlUnit(connection)

                # Sleep, because for some reason the Arduino resets itself after connecting with PySerial?
                print("Booting....")
                sleep(3)

                # Identify
                unit_id = control_unit.identify()

                if unit_id in CONTROL_UNITS:
                    control_unit.name = CONTROL_UNITS[unit_id]['name']
                    control_unit.update_interval = CONTROL_UNITS[unit_id]['update_interval']
                    control_unit.id = unit_id

                    # This is a control unit! Adding it!
                    self.control_units.append(control_unit)

                    if self.on_cu_connect_cb:
                        self.on_cu_connect_cb(control_unit)
                else:
                    print("Not a control unit :(")

            except (OSError, serial.SerialException):
                print("Could not connect to this port!")

    def on_cu_connect(self, cb):
        self.on_cu_connect_cb = cb

    def on_cu_disconnect(self, cb):
        self.on_cu_disconnect_cb = cb

    def on_cu_update(self, cb):
        self.on_cu_update_cb = cb

    @staticmethod
    def get_open_ports():
        # https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
        comlist = serial.tools.list_ports.comports()
        connected = []

        if sys.platform.startswith('win'):
            for element in comlist:
                if 'COM' in element.device:
                    connected.append(element.device)
        else:
            for element in comlist:
                if 'usb' in element.device:
                    connected.append(element.device)

        return connected
