class ControlUnit:
    def __init__(self, connection):
        self.id = None
        self.connection = connection
        self.name = None
        self.update_interval = 0

    # Write one byte
    def write(self, value):
        byte = int(value).to_bytes(1, 'big')
        self.connection.write(byte)

    # Read one byte
    def read(self):
        value = self.connection.read()

        if value:
            return int(value.hex(), 16)

        return -1

    # Checks if the control unit is still connected
    def is_connected(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            return False

    '''
    This function sends the number 0x00 to the ControlUnit (the Arduino)
    Then the ControlUnit executes the function identify() in the communication-functions.c file
    That functions writes 0x01
    We read that value and return it!
    '''

    # 0x00
    def identify(self):
        self.write(0x00)
        return self.read()


    # 0x01
    def read_sensor(self):
        self.write(0x01)
        return self.read()

    # 0x02
    def pull_in(self):
        self.write(0x02)
        return self.read()

    # 0x03
    def pull_out(self):
        self.write(0x03)
        return self.read()

    # 0x04
    def set_lower_limit(self, value):
        self.write(0x04)
        self.write(value)
        return self.read()

    # 0x05
    def set_upper_limit(self, value):
        self.write(0x05)
        self.write(value)
        return self.read()

    # 0x06
    def set_max_pull_out(self, value):
        self.write(0x06)
        self.write(value)
        return self.read()
