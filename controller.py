from tkinter import *
from tkinter import ttk
from time import sleep
from SerialCommunication import ControlUnitManager
import threading
from queue import Queue


class Controller:
    def __init__(self, main_thread):
        self.CONNECT = 'connect'
        self.DISCONNECT = 'disconnect'
        self.UPDATE = 'update'

        self.main_thread = main_thread
        self.manager = ControlUnitManager()

        self.manager.on_cu_connect(self.onConnect)
        self.manager.on_cu_disconnect(self.onDisconnect)
        self.manager.on_cu_update(self.onUpdate)

    def run(self):
        while True:
            self.manager.update()
            sleep(1)

    def onUpdate(self, control_unit, value):
        self.main_thread.queue.put({self.UPDATE: [control_unit.id, value]})

    def onConnect(self, control_unit):
        print("Connected to {0}".format(control_unit.name))
        self.main_thread.queue.put({self.CONNECT: [control_unit.id]})

    def onDisconnect(self, control_unit):
        print("Disconnected from {0}".format(control_unit.name))
        self.main_thread.queue.put({self.DISCONNECT: [control_unit.id]})

    def set_upper_limit(self, unit_id, value):
        for unit in self.manager.control_units:
            if unit.id == unit_id:
                unit.set_upper_limit(value)

    def set_lower_limit(self, unit_id, value):
        for unit in self.manager.control_units:
            if unit.id == unit_id:
                unit.set_lower_limit(value)

    def set_max_pull_out(self, unit_id, value):
        for unit in self.manager.control_units:
            if unit.id == unit_id:
                unit.set_max_pull_out(value)

    def pull_in(self, unit_id):
        for unit in self.manager.control_units:
            if unit.id == unit_id:
                unit.pull_in()

    def pull_out(self, unit_id):
        for unit in self.manager.control_units:
            if unit.id == unit_id:
                unit.pull_out()



