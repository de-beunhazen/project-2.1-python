from tab import Tab
from tkinter import *
from tkinter import ttk
from time import sleep
from controller import Controller
import threading
from queue import Queue


class Main:
    def __init__(self, root):
        self.queue = Queue()
        self.root = root
        self.notebook = ttk.Notebook(self.root)

        self.tabs = {}

        self.tab1 = Tab('Control Unit 1', self.notebook, 1)
        self.tabs[self.tab1.id] = self.tab1
        self.tab2 = Tab('Control Unit 2', self.notebook, 2)
        self.tabs[self.tab2.id] = self.tab2
        self.tab3 = Tab('Control Unit 3', self.notebook, 3)
        self.tabs[self.tab3.id] = self.tab3
        self.tab4 = Tab('Control Unit 4', self.notebook, 4)
        self.tabs[self.tab4.id] = self.tab4
        self.tab5 = Tab('Control Unit 5', self.notebook, 5)
        self.tabs[self.tab5.id] = self.tab5

        self.controller = Controller(self)
        self.controller_thread = threading.Thread(target=self.controller.run)
        self.controller_thread.start()

        self.tab1.addGraph('Temperatuur (C)')
        self.tab2.addGraph('Lichtintensiteit (%)')

        self.tab1.addInput('max_pull_out', 'Maximale uitrol lengte', 0, 20)
        self.tab1.addInput('upper_limit', 'Bovengrens', 1, 25)
        self.tab1.addInput('lower_limit', 'Ondergrens', 2, 24)
        self.tab1.addButton('Opslaan', lambda: self.sendData(self.tab1), 192, 90, 21)
        self.tab1.addButton('Rol uit', lambda: self.pullOut(self.tab1), 10, 750, 21)
        self.tab1.addButton('Rol in', lambda: self.pullIn(self.tab1), 210, 750, 21)

        self.tab2.addInput('max_pull_out', 'Maximale uitrol lengte', 0, 20)
        self.tab2.addInput('upper_limit', 'Bovengrens', 1, 80)
        self.tab2.addInput('lower_limit', 'Ondergrens', 2, 40)
        self.tab2.addButton('Opslaan', lambda: self.sendData(self.tab2), 192, 90, 21)
        self.tab2.addButton('Rol uit', lambda: self.pullOut(self.tab2), 10, 750, 21)
        self.tab2.addButton('Rol in', lambda: self.pullIn(self.tab2), 210, 750, 21)

        self.notebook.pack()
        self.periodicCall()

    def periodicCall(self):
        self.processQueue()
        self.root.after(1000, self.periodicCall)

    def sendData(self, tab):
        inputs = tab.getInputs()
        self.controller.set_max_pull_out(tab.id, inputs['max_pull_out'].get())
        self.controller.set_upper_limit(tab.id, inputs['upper_limit'].get())
        self.controller.set_lower_limit(tab.id, inputs['lower_limit'].get())

    def pullIn(self, tab):
        self.controller.pull_in(tab.id)

    def pullOut(self, tab):
        self.controller.pull_out(tab.id)

    def processQueue(self):
        while self.queue.qsize():
            try:
                values = self.queue.get()
                for key, value in values.items():
                    if key == self.controller.UPDATE:
                        self.updateTab(value)
                    elif key == self.controller.CONNECT:
                        self.connectTab(value)
                    else:
                        self.disconnectTab(value)
            except Exception as e:
                print(e)

    def updateTab(self, values):
        tab = self.tabs[values[0]]
        value = values[1]
        tab.addData(value)

    def connectTab(self, value):
        tab = self.tabs[value[0]]
        tab.setState("normal")
        self.notebook.select(tab.getFrame())
        self.sendData(tab)

    def disconnectTab(self, value):
        tab = self.tabs[value[0]]
        tab.setState("disabled")

        for key, tab in self.tabs.items():
            if tab.getState() != 'disabled':
                print()


root = Tk()
root.title('De centrale')
root.geometry("1200x900")

mainThread = Main(root)
mainThread.root.mainloop()
