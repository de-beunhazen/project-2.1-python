from tkinter import *
from tkinter import ttk
from time import sleep
from datetime import datetime
import matplotlib.dates as mdates
import pytz
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import tkinter
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.text as mtext
import matplotlib.animation as anim
from matplotlib.ticker import FixedLocator, FixedFormatter


class Tab:

    def __init__(self, title, notebook, tab_id):
        self.graph_data = tuple()
        self.id = tab_id
        self.figure = Figure(figsize=(7, 5), dpi=100)
        self.ax = None
        self.current_annotation = None
        self.time = 0
        self.timezone = pytz.timezone('Europe/Amsterdam')
        self.x_data = []
        self.y_data = []
        self.inputs = {}
        self.state = "disabled"
        self.notebook = notebook
        self.frame = ttk.Frame(self.notebook)

        self.graph_window = PanedWindow(self.frame, width=499)
        self.graph_window.pack(side="left", expand=1, fill="both")
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.graph_window)

        separator = PanedWindow(self.frame, width=2, background='gray60')
        separator.pack(side="left", expand=0, fill="both")

        self.setting_window = PanedWindow(self.frame, width=299)
        self.setting_window.pack(side="right", expand=1, fill="both")

        self.notebook.add(self.frame, text=title)
        self.notebook.pack(expand=1, fill="both")
        # self.notebook.tab(tab_id - 1, state="disabled")

    def addGraph(self, label):
        self.ax = self.figure.add_subplot(111, xlabel='Time (H:M:S)', ylabel=label)
        self.ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        self.figure.autofmt_xdate(rotation=45)
        self.graph_data, = self.ax.plot(datetime.now(self.timezone), 0, '-r')
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

    def addInput(self, type, label, order, default):
        Label(self.setting_window, text=label, padx=20).grid(sticky="W", row=order)
        entry = Entry(self.setting_window)
        entry.grid(row=order, column=1, sticky="E")
        entry.insert(END, default)
        entry.grid(row=order, column=1)
        self.inputs[type] = entry

    def addButton(self, label, callback, x, y, width=24):
        Button(self.setting_window, text=label, command=callback, pady=8, width=width).place(x=x, y=y)

    def getData(self):
        return self.graph_data

    def addData(self, value):
        self.y_data.append(value)
        self.x_data.append(datetime.now(self.timezone))
        self.updateGraph()

    def annot_max(self, xmax, ymax):
        if self.current_annotation:
            self.current_annotation.remove()

        text = "Hoogste waarde = {:.3f}".format(ymax)
        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        arrowprops = dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=60")
        kw = dict(xycoords='data', textcoords="axes fraction",
                  arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
        self.current_annotation = self.ax.annotate(text, xy=(xmax, ymax), xytext=(0.94, 0.96), **kw)

    def updateGraph(self):
        self.ax.plot(self.x_data, self.y_data, '-r')

        ymax = max(self.y_data)
        xpos = self.y_data.index(ymax)
        xmax = self.x_data[xpos]

        self.annot_max(xmax, ymax)
        self.ax.set_ylim(0, ymax + 40)

        self.canvas.draw()
        self.canvas.flush_events()

    def getInputs(self):
        return self.inputs

    def getFrame(self):
        return self.frame

    def setState(self, value):
        self.notebook.tab(self.id - 1, state=value)
        self.state = value

    def getState(self):
        return self.state


